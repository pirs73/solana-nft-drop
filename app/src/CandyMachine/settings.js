import { createUmi } from '@metaplex-foundation/umi-bundle-defaults';
import { mplCandyMachine } from '@metaplex-foundation/mpl-candy-machine';
import { mplBubblegum } from '@metaplex-foundation/mpl-bubblegum';
import {
  percentAmount,
  generateSigner,
  some,
  none,
} from '@metaplex-foundation/umi';
import {
  TokenStandard,
  createNft,
} from '@metaplex-foundation/mpl-token-metadata';

const umi = createUmi('https://api.devnet.solana.com').use(mplCandyMachine());
umi.use(mplBubblegum());

// Create the Collection NFT.
const collectionUpdateAuthority = generateSigner(umi);
const collectionMint = generateSigner(umi);
await createNft(umi, {
  mint: collectionMint,
  authority: collectionUpdateAuthority,
  name: 'My Collection NFT',
  uri: '../assets/metadata.json',
  sellerFeeBasisPoints: percentAmount(9.99, 2), // 9.99%
  isCollection: true,
}).sendAndConfirm(umi);

const myCustomAuthority = generateSigner(umi);

const creatorA = generateSigner(umi).publicKey;
export const candyMachineSettings = {
  authority: myCustomAuthority,
  tokenStandard: TokenStandard.NonFungible,
  sellerFeeBasisPoints: percentAmount(33.3, 2),
  symbol: 'DOGSNFT',
  maxEditionSupply: 0,
  isMutable: true,
  creators: [{ address: creatorA, percentageShare: 100, verified: false }],
  collectionMint: collectionMint.publicKey,
  collectionUpdateAuthority,
  itemsAvailable: 5000000000,
  hiddenSettings: none(),
  configLineSettings: some({
    prefixName: 'My NFT Project #$ID+1$',
    nameLength: 0,
    prefixUri: 'https://arweave.net/',
    uriLength: 43,
    isSequential: false,
  }),
};
